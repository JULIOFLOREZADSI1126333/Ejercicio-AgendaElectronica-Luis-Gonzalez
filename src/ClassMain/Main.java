package ClassMain;
import Procesos.*;// portamos las clases del paquete procesos
import java.util.Scanner;

public class Main {
	public static void main(String [] args)
	{
           Scanner data = new Scanner(System.in);//objeto para captura de lemento por teclado
           //objeto persona de tipo GestionContacto
           Contacto persona = new GestionContacto();
           System.out.println(("agenda electronica").toUpperCase());
           System.out.println("");
           System.out.println("Desea Iniciar La Agenda");
           String opE = data.next();
           System.out.println("");
           while(opE.equals("si"))
           {
                System.out.println("Digite R si desea registrar");
                System.out.println("Digite C si desea consultar");
                System.out.println("Digite A si desea actualizar");
                System.out.println("");
                String op = data.next();
                if(op.equals("R"))
                {
                    System.out.println("");
                    System.out.println("Ingrese la cantidad de registros a registrar:");
                    persona.setCantidadR(data.nextInt());
                    persona.Insercion();
                }
                else if(op.equals("C"))
                {
                    System.out.println("");
                    System.out.println("Digite G si desea una consulta general");
                    System.out.println("Digite E si desea una consulta especifica");
                    persona.setopConsulta(data.next());
                    persona.Consultar();// se ejecuta el metodo consultar  de tipo GestionContacto
                }
                else if(op.equals("A"))
                {
                    System.out.println(("¿Desea realizar alguna actualizacion y ver los registro en mayusculas y munusculas con"
                        + " la cantidad de carateres correspondientes?").toUpperCase());
                    persona.setopActualizacion(data.next());//set de recolipalacion de dato para el parametro
                    persona.Actualizar();  //se ejecuta el meto Actualizar de tipo GestionContacto*/
                }
                else if(op!="R"||op!="C"||op!="A")
                {
                    System.out.println("La opción ingresada no es valida");              
                }
                System.out.println("");
                System.out.println("Desea Continuar Usando la agenda");
                opE = data.next();
           }
	}
}
