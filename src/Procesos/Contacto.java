package Procesos;
//clase abstracta
public abstract class Contacto {
    
    //atributos de validadcion
    String opConsulta = "";
    String opActualizacion = "";
    int cantidadR = 0;
    
    
    //Metodos abstractos
    public abstract void Insercion();
    public abstract void Consultar();
    public abstract void Actualizar(); 
    
    // Get y Set de la variable opConsulta
    public void setopConsulta(String opConsulta) {
        this.opConsulta = opConsulta;
    }
    public String getopConsultan() {
        return opConsulta;
    }

    // Get y Set de la variable opActualizacion
    public void setopActualizacion(String opActualizacion) {
        this.opActualizacion = opActualizacion;
    }
    public String getopActualizacion() {
        return opActualizacion;
    }
    
    /**
     * @return the cantidadR
     */
    public int getCantidadR() {
        return cantidadR;
    }

    /**
     * @param cantidadR the cantidadR to set
     */
    public void setCantidadR(int cantidadR) {
        this.cantidadR = cantidadR;
    }
}
