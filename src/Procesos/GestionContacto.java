package Procesos;


import java.util.Scanner;

public class GestionContacto extends Contacto{
    
    String [][] BD = new String[3][5];//vector que almacena datos
    Scanner data = new Scanner(System.in);//objeto para la obetecion de carateres d entrada
    
    //variables privadas para validaciones de entrada
    private int Nregistro = 0;
    private String Ncolumna = "";
    private String valorAct = "";
    boolean reexitoso = false;
    
    //sobreescritura del metodo Insercion
    @Override
    public void Insercion()
    {
        //matriz para registar datos
            for (int f = 0; f <cantidadR; f++) 
            {
                //se genera el numero del registro
                int nr=0;
                nr=f+1;
                System.out.println("");
                System.out.println("");
                System.out.println("REGISTRO NUMERO: "+nr);
                System.out.println("");
                
                for (int c = 0; c <5; c++) 
                {
                    if(c==0)
                    {
                        System.out.println("Ingrese el nombre del nuevo contacto:");//mensaje aparece cuando el contador esta siempre en la posicion 0
                        BD[f][c] = data.next();//rgistra datos en esa posicion
                    }
                    else
                    {
                        if(c==1)
                        {
                            System.out.println("Ingrese el apellido del nuevo contacto:");//mensaje aparece cuando el contador esta siempre en la posicion 1
                            BD[f][c] = data.next();
                        }
                        else
                        {
                            if(c==2)
                            {
                                System.out.println("Ingrese la edad del nuevo contacto:");//mensaje aparece cuando el contador esta siempre en la posicion 2
                                BD[f][c] = data.next();
                            }
                            else
                            {
                                if(c==3)
                                {
                                    System.out.println("Ingrese el celular del nuevo contacto:");//mensaje aparece cuando el contador esta siempre en la posicion 3
                                    BD[f][c] = data.next();
                                }
                                else
                                {
                                    if(c==4)
                                    {
                                        System.out.println("Ingrese el correo del nuevo contacto:");//mensaje aparece cuando el contador esta siempre en la posicion 3
                                        BD[f][c] = data.next();
                                    }
                                }
                            }
                        }
                    }
                }
                reexitoso=true;
            }
     }


    //sobre escritura metodo consultar
    @Override
    public void Consultar()
    {
        boolean encontro = false;
        if (opConsulta.equals("G")) {
            System.out.println(" "+"N°REGISTRO          "+" "+"NOMBRE          "+"APELLIDO          "+"EDAD          "+"CELULAR          "+"CORREO");// mensaje de encabezado

                    for (int f = 0; f <cantidadR; f++) 
                    {
                            System.out.print(" "+f+"                    ");
                            for (int c = 0; c <5; c++) 
                            {
                                    System.out.print(" "+BD[f][c]+"         ");//se imprime la matriz
                            }
                            System.out.println();//salto de line para la siguiente fila
                    }
        }
        else
        {
            if (opConsulta.equals("E")) {
                System.out.println("Ingrese el nombre del contacto para consulta el registro de este");
                String nom = data.next();
                System.out.println("");
                System.out.println(" "+"NOMBRE          "+"APELLIDO          "+"EDAD          "+"CELULAR          "+"CORREO");// mensaje de encabezado
                for (int f = 0; f <cantidadR; f++) 
                    {
                            for (int c = 0; c <5; c++) 
                            {
                                if ((BD[f][c]).equals(nom)) {
                                    for(int cc = 0; cc <5; cc++)
                                    {
                                      System.out.print(" "+BD[f][cc]+"         ");   
                                      encontro = true;
                                    }
                                }                                
                            }
                            System.out.println();//salto de line para la siguiente fila
                    }
                if(encontro == false)
                {
                    System.out.println("Noy hay registro con ese nombre");
                }   
            }
        }
    }
    
    //metodo sobrescrito Actualizar
    @Override
    public void Actualizar()
    {
        if(reexitoso==false)
        {
            System.out.println("No hay registros a actualizar");
        }
        else
        {
            if(opActualizacion.equals("si")) //se evalua la opcion de la variable opActualizacion
            {
                System.out.println("Digite el numero del registro a actualizar:");
                setNregistro( Integer.parseInt(data.next())); //encapsula dato y lo pasa a int
                System.out.println("Digite el nombre de la columna a actualizar (el nombre en minuscula):");
                setNcolumna(data.next()); // encapsula para el atributo Ncolumna
                System.out.println("Digite el nuevo valor para actualizar:");
                setValorAct(data.next());//encapsula en nuevo valor
                //valida segun el nombre de la columna y le asiga un valor
                if (Ncolumna.equals("nombre")) {
                    BD[Nregistro][0] = valorAct; // roma el valor de actualizacion y lo coloca en esa posicion
                }
                else if(Ncolumna.equals("apellido"))
                {
                    BD[Nregistro][1] = valorAct;
                }
                else if(Ncolumna.equals("edad"))
                {
                    BD[Nregistro][2] = valorAct;
                }
                else if(Ncolumna.equals("celular"))
                {
                    BD[Nregistro][3] = valorAct;
                }
                else if(Ncolumna.equals("correo"))
                {
                    BD[Nregistro][4] = valorAct;
                }
                //mensajes
                System.out.println("");
                System.out.println(("-------------- ¡Actualizacion Exitoso! --------------").toUpperCase());//toUpperCase() para convertir en mayuscula
                System.out.println("");
                System.out.println(("Registros actualizados en mayuscula").toUpperCase());
                System.out.println(" "+"N°REGISTRO          "+" "+"NOMBRE          "+"APELLIDO          "+"EDAD          "+"CELULAR          "+"CORREO");//encabezado de tabla

                for (int f = 0; f <cantidadR; f++) 
                {
                        System.out.print(" "+f+"                    ");// me imprime el numero del registro
                        for (int c = 0; c <5; c++) 
                        {
                                System.out.print(" "+(BD[f][c]).toUpperCase()+"         ");//toUpperCase() utilizado para convertir cadenas de caracteres en mayusculas
                        }
                        System.out.println();
                }

                System.out.println("");
                System.out.println(("Registros actualizados en minuscula").toUpperCase());
                System.out.println(" "+"N°REGISTRO          "+" "+"NOMBRE          "+"APELLIDO          "+"EDAD          "+"CELULAR          "+"CORREO");//encabezado tabla
                for (int f = 0; f <cantidadR; f++) 
                {
                        System.out.print(" "+f+"                    ");// me imprime el numero del registro
                        for (int c = 0; c <5; c++) 
                        {
                                System.out.print(" "+(BD[f][c]).toLowerCase()+"         ");//toLowerCase() utilizado para convertir cadenas de caracteres en minusculas
                        }
                        System.out.println();//salta de linea para iniciar otra fila
                }
                System.out.println("");
                System.out.println(("Numero de caracteres en cada atributo del registro").toUpperCase());
                System.out.println(" "+"N°REGISTRO           "+" "+"NOMBRE   "+"APELLIDO       "+"EDAD       "+"CELULAR       "+"CORREO");//ecabezado tabla

                for (int f = 0; f <cantidadR; f++) 
                {
                        System.out.print(" "+f+"                     ");// me imprime el numero del registro
                        for (int c = 0; c <5; c++) 
                        {
                            if(c==0 || c==1 || c==4)
                            {
                                System.out.print(" "+(BD[f][c]).length()+"          ");//lengh() se utiliza para contar los caracteres de un string   
                            }
                            else
                            {
                                System.out.print(" "+(BD[f][c])+"          ");//no cuenta caracteres de las columnas 2 y 3 (edad y celular)
                            }
                        }
                        System.out.println();//salta de linea para iniciar otra fila
                }

            }
        }
    }

    //set y get para numero registro
    public int getNregistro() {
        return Nregistro;
    }

    public void setNregistro(int Nregistro) {
        this.Nregistro = Nregistro;
    }

    //set y get para Nombre columna
    public String getNcolumna() {
        return Ncolumna;
    }

    public void setNcolumna(String Ncolumna) {
        this.Ncolumna = Ncolumna;
    }

    //set y get para valor actual
    public String getValorAct() {
        return valorAct;
    }

    public void setValorAct(String valorAct) {
        this.valorAct = valorAct;
    }

    
}
